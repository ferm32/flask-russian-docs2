Руководство пользователя
------------------------

Эта, в основном скучная, часть документации, начинается с некой вводной
инфомацией о Flask, а затем фокусируется на пошаговых инструкциях по
веб-разработке вместе с Flask.

.. toctree::
   :maxdepth: 2

   foreword
   advanced_foreword
   installation
   quickstart
   tutorial/index
   templating
   testing
   errorhandling
   logging
   config
   signals
   views
   appcontext
   reqcontext
   blueprints
   extensions
   cli
   server
   shell
   patterns/index
   deploying/index
   becomingbig

Справочник по API
-----------------

Если вам нужна информация по конкретной функции, классу, или методу,
то этот раздел документации для вас.

.. toctree::
   :maxdepth: 2

   api

Дополнительные заметки
----------------------

Для тех, кому инетерсно - заметки по дизайну, правовая информация и история
изменений.

.. toctree::
   :maxdepth: 2

   design
   htmlfaq
   security
   unicode
   extensiondev
   styleguide
   upgrading
   changelog
   license
   contributing
