.. _installation:

Инсталляция
===========

Версия Python
-------------

Рекомендуем использовать последнюю версию Python 3. Flask поддерживает Python
3.4 и новее, Python 2.7 и PyPy.

Зависимости
-----------

При установке Flask, следующее программное обеспечение будет установлено
автоматически:

* `Werkzeug`_ реализует WSGI, стандартный интерфейс Python между приложениями и
  серверами.
* `Jinja`_ это язык шаблонов, который формирует страницы, выдаваемые вашим
  приложением.
* `MarkupSafe`_ идёт вместе с Jinja. Он помогает избежать атак через внедрение
  с помощью экранирования недоверенных входных данных при отрисовке шаблонов.
* `ItsDangerous`_ безопасно подписывает данные для уверенности в их
  целостности. Используется для защиты во Flask его сессионных cookie.

* `Click`_ это фреймворк для написания приложений командной строки. Он
  обеспечивает работу команды ``flask`` и позволяет добавлять пользовательские
  команды управления.

.. _Werkzeug: http://werkzeug.pocoo.org/
.. _Jinja: http://jinja.pocoo.org/
.. _MarkupSafe: https://pypi.org/project/MarkupSafe/
.. _ItsDangerous: https://pythonhosted.org/itsdangerous/
.. _Click: http://click.pocoo.org/

Необязательные зависимости
~~~~~~~~~~~~~~~~~~~~~~~~~~

Эти наборы программного обеспечения не будут установлены автоматически.
Если Вы их установите, Flask поймёт это и начнёт использовать.

* `Blinker`_ обеспечивает поддержку для: :ref:`signals`.
* `SimpleJSON`_ это быстрая реализация JSON, которая совместима с модулем
  Python ``json``. Если она установлена, то будет предпочитаться при выполнении
  операций с JSON.
* `python-dotenv`_ включает поддержку для :ref:`dotenv`при запуске команд
  ``flask``.
* `Watchdog`_ обеспечивает для сервера разработки более быстрый и эффективный
  "перезагрузчик".

.. _Blinker: https://pythonhosted.org/blinker/
.. _SimpleJSON: https://simplejson.readthedocs.io/
.. _python-dotenv: https://github.com/theskumar/python-dotenv#readme
.. _watchdog: https://pythonhosted.org/watchdog/

Виртуальные окружения
---------------------

Для управления зависимостями как при разработке, так и в продуктивной среде,
следует использовать виртуальные окружения.

Какую проблему разрешает виртуальное окружение? Чем у вас больше проектов
на Python, тем больше вероятность, что вам нужно работать с разными версиями
библиотек Python или даже самого Python. Более новые версии библиотек для
одного проекта могут разрушить совместимость в другом проекте.

Виртуальные окружения это независимые группы библиотек Python, по одному
для каждого проекта. Пакеты, установленные для одного проекта, не имеют
влияния на другие проекты или на пакеты операционной системы.

В комплекте с Python 3 идёт модуль :mod:`venv` для создания виртуальных
окружений. Если вы используете современные версии Python, то можете перейти
к следующему разделу.

Если у вас Python 2, смотрите сначала
:ref:`Инсталляция-Инсталляция-virtualenv`.

.. _install-create-env:

Создание окружения
~~~~~~~~~~~~~~~~~~

Создайте папку проекта и подпапку :file:`venv` внутри неё:

.. code-block:: sh

    mkdir myproject
    cd myproject
    python3 -m venv venv

Для Windows:

.. code-block:: bat

    py -3 -m venv venv

Если вы используете Python 2, и вам нужно инсталлировать virtualenv, тогда
вместо этого используете следующие команды:

.. code-block:: sh

    python2 -m virtualenv venv

Для Windows:

.. code-block:: bat

    \Python27\Scripts\virtualenv.exe venv

.. _install-activate-env:

Активация окружения
~~~~~~~~~~~~~~~~~~~

До работы с вашим проектом, активируйте соответствующее окружение:

.. code-block:: sh

    . venv/bin/activate

Для Windows:

.. code-block:: bat

    venv\Scripts\activate

Приглашение командной оболочки сменится и теперь будет отображать имя
активированного окружения.

Инсталляция Flask
-----------------

Чтобы инсталлировать Flask, используйте следующую команду внутри
активированного окружения:

.. code-block:: sh

    pip install Flask

Теперь Flask установлен. Переходите к разделу :doc:`/quickstart` или
:doc:`Documentation Overview </index>`.

Жизнь на острие
~~~~~~~~~~~~~~~

Если вам хочется работать с самым последним кодом Flask ещё до того, как
он выпущен в виде релиза, установите или обновите код из ветки master:

.. code-block:: sh

    pip install -U https://github.com/pallets/flask/archive/master.tar.gz

.. _install-install-virtualenv:

Инсталляция virtualenv
----------------------

Если у вас Python 2, то модуль venv недоступен. Вместо него установите
`virtualenv`_.

Для Linux, virtualenv устанавливается с помощью вашего менеджера пакетов:

.. code-block:: sh

    # Debian, Ubuntu
    sudo apt-get install python-virtualenv

    # CentOS, Fedora
    sudo yum install python-virtualenv

    # Arch
    sudo pacman -S python-virtualenv

Если у вас Mac OS X или Windows, сначала загрузите `get-pip.py`_, затем
выполните:

.. code-block:: sh

    sudo python2 Downloads/get-pip.py
    sudo python2 -m pip install virtualenv

Для Windows, выполните из-под учётной записи администратора:

.. code-block:: bat

    \Python27\python.exe Downloads\get-pip.py
    \Python27\python.exe -m pip install virtualenv

Теперь можете вернуться выше и выполнить :ref:`install-create-env`.

.. _virtualenv: https://virtualenv.pypa.io/
.. _get-pip.py: https://bootstrap.pypa.io/get-pip.py
